import sys
sys.path.append('../../allgo')

import allgo as ag
print(ag.__version__)
print(ag.__file__)

import imageio
import matplotlib.pyplot as plt
from ipywidgets import interact_manual
from PIL import Image
from skimage import io
import numpy as np
import ipyvolume as ipv
import glob

import ntpath
from IPython.display import clear_output

clear_output()
print("Operation finished")
