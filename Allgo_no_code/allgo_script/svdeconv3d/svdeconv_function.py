output_image = None

def run_svdeconv3d(regularization, weighting, method, psf):
    global output_image

    input_basename = ntpath.basename(input_image_path)
    files = [ input_image_path ]
    params = '-regularization ' + str(regularization) + ' -weighting ' + str(weighting) + ' -method ' + str(method)
    out_dict1 = client.run_job('Svdeconv3d', files = files, params = params)
    job_id = out_dict1['id']

    url = out_dict1[str(job_id)][input_basename + '']
    filepath = client.download_file(file_url = url, outdir = "../Output" , force = True)
    output_image = imageio.imread(filepath)

    print("Operation finished, {}".format(filepath))
    
interact_manual(run_svdeconv3d, regularization = (1,60), weighting = (0.1, 0.9, 0.1), method = ['SV', 'HSV'], psf = psf_image_path)
