output_image = None

def run_kltracker(number, bits):
    global output_image

    input_basename = ntpath.basename(input_image_path)
    files = [ input_image_path ]
    params = '-nF ' + str(number) + ' -bits ' + str(bits)
    out_dict1 = client.run_job('Kltracker', files = files, params = params)
    job_id = out_dict1['id']
    url = out_dict1[str(job_id)][input_basename + '.output.tif.tif']
    print(out_dict1)
    print("job_id = ", job_id)
    
    outdir = '../Output'
    filepath = client.download_file(file_url = url, outdir = outdir , force = True)
    print(filepath)
    output_image = imageio.imread(filepath)

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image)
    plt.show()
    
interact_manual(run_kltracker, number = (1, 200) , bits = (0,16, 8))

