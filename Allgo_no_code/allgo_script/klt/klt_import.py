import sys
sys.path.append('../../allgo')

import allgo as ag
print(ag.__version__)
print(ag.__file__)

from PIL import Image
import numpy as np
import glob
import cv2

import imageio
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from ipywidgets import interact_manual

import ntpath
from IPython.display import clear_output

clear_output()
