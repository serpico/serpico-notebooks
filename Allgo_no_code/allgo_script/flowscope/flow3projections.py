w = 10
h = 10
fig = plt.figure(figsize=(8, 8))

columns = 2
rows = 2
for i in range(1, 4):
    fig.add_subplot(rows, columns, i)
    if i == 1 :
            plt.imshow(colors_XY)
            plt.axvline(x=x,color='red')
            plt.axhline(y=y,color='red')
    if i == 2 :
        plt.imshow(colors_YZ)
        plt.axvline(x=z,color='red')
        plt.axhline(y=y,color='red')
    if i == 3 :
        plt.imshow(colors_XZ)
        plt.axvline(x=x,color='red')
        plt.axhline(y=z,color='red')
plt.show()

print("Top left : XY axis")
print("Down left : XZ axis")
print("Top right : YZ axis")
