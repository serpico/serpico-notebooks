output_image = None
filepath1 = None
filepath2 = None
filepath3 = None

def run_flowscope(gamma, maximum_iteration):
    global output_image
    global filepath1
    global filepath2
    global filepath3
    
    input_basename1 = ntpath.basename(input_image_path1)
    input_basename2 = ntpath.basename(input_image_path2)
    input_basename3 = (ntpath.basename(input_image_path1) + "_" + ntpath.basename(input_image_path2))

    files = {input_image_path1 : open(input_image_path1, 'rb'), input_image_path2 : open(input_image_path2, 'rb')}
    params = " -s " + str(input_basename1) + " -t " + str(input_basename2) + " -o" + str(" .") + " -g" + str(gamma) + " -i" + str(maximum_iteration)
    out_dict1 = client.run_job('Flowscope', files = files, params = params)
    job_id = out_dict1['id']
    
    url1 = out_dict1[str(job_id)][input_basename3 + "soru.tif"]
    url2 = out_dict1[str(job_id)][input_basename3 + "sorv.tif"]
    url3 = out_dict1[str(job_id)][input_basename3 + "sorw.tif"]

    outdir = "../Output"
    
    filepath1 = client.download_file(file_url = url1, outdir = outdir, force = True)
    filepath2 = client.download_file(file_url = url2, outdir = outdir, force = True)
    filepath3 = client.download_file(file_url = url3, outdir = outdir, force = True)
    
    
    print("Job done : your output files are in the 'Output' folder")
    print(filepath1)
    print(filepath2)
    print(filepath3)

interact_manual(run_flowscope, gamma = (0, 2.4), maximum_iteration = (0,12))
