# Interactive parameters selection
output_image = None

def run_hotspotdetection(patch, neighborhood, p_value):
    global output_image

    files = [ input_image_path ]
    params = '-m ' + str(patch) + ' -n ' + str(neighborhood) + ' -pv ' + str(p_value)
    out_dict1 = client.run_job('Hotspotdetection', files = files, params = params)
    job_id = out_dict1['id']
    
    url = out_dict1[str(job_id)]['output.tif']
    outdir = "../Output"
    filepath = client.download_file(file_url = url, outdir = outdir , force = True)
    output_image = imageio.imread(filepath)

    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image)
    plt.show()
    
interact_manual(run_hotspotdetection, patch = (1, 6) , neighborhood = (0,10), p_value = (0,0.4,0.001))

