# Interactive parameters selection
output_image = None

def run_atlas(rad, pval):
    global output_image
    
    input_basename = ntpath.basename(input_image_path)
    files = [ input_image_path ]
    params = '-rad ' + str(rad) + ' -pval ' + str(pval)
    out_dict1 = client.run_job('Atlas', files = files, params = params)
    job_id = out_dict1['id']
    url = out_dict1[str(job_id)][input_basename + '.output.tif']
    
    outdir = "../Output"
    filepath = client.download_file(file_url = url, outdir = outdir , force=True)
    output_image = imageio.imread(filepath)

    print("You will find the results in the Ouput file")
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap = "gist_gray")
    plt.show()
    
interact_manual(run_atlas, rad = (1, 40) , pval = (0.01,1,0.001))

