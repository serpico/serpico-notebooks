output_image = None

def run_svdeconv(sigma, regularization, weighting, method):
    global output_image

    input_basename = ntpath.basename(input_image_path)
    files = [ input_image_path ]
    params = '-sigma ' + str(sigma) + ' -regularization ' + str(regularization) + ' -regularization ' + str(regularization) + ' -weighting ' + str(weighting) + ' -method ' + str(method)
    out_dict1 = client.run_job('Svdeconv', files = files, params = params)
    job_id = out_dict1['id']
    
    url = out_dict1[str(job_id)][input_basename + '.output.tif']
    outdir = "../Output"
    filepath = client.download_file(file_url = url, outdir = outdir, force = True)
    output_image = imageio.imread(filepath)

    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap = "gist_gray")
    plt.show()
    
interact_manual(run_svdeconv, sigma = (1.0, 4.0, 0.5) , regularization = (1,22) , weighting = (0.1, 0.9, 0.1) , method = ["SV","HSV"])

