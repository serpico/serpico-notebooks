output_image = None

def run_ndsafir(patch,iteration):
    global output_image

    files = [ input_image_path ]
    params = '-patch ' + str(patch) + ' -iter ' + str(iteration) + ' -algo SAFIR'
    out_dict1 = client.run_job('Cimgdenoising', files = files, params = params)
    job_id = out_dict1['id']
    url = out_dict1[str(job_id)]['output.tif']
    
    outdir = "../Output"
    filepath = client.download_file(file_url = url, outdir = outdir, force = True)
    output_image = imageio.imread(filepath)

    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap = "gist_gray")
    plt.show()
    
interact_manual(run_ndsafir, patch = (1, 4) , iteration = (1,7))

