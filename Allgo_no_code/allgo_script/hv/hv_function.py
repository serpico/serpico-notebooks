output_image = None

def run_sv(denoisep,sparsep):
    global output_image

    files = [ input_image_path ]
    params = '-denoisep ' + str(10*denoisep) + ' -sparsep ' + str(sparsep) + ' -algo HV'
    out_dict1 = client.run_job('Cimgdenoising', files = files, params = params)
    job_id = out_dict1['id']
    
    url = out_dict1[str(job_id)]['output.tif']
    outdir = "../Output"
    filepath = client.download_file(file_url = url, outdir = outdir , force = True)
    output_image = imageio.imread(filepath)

    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap = "gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap = "gist_gray")
    plt.show()
    
interact_manual(run_sv, denoisep=(0, 10) , sparsep=(0.1,0.9,0.01))

