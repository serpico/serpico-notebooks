output_image=None
def run_ndsafir(patch,iteration):
    global output_image
    myprocess=BiProcess('../../toolshed/cimgdenoising/Denoising_SAFIR.xml')
    output_image=myprocess.run('-i', input_image,
                '-patch', patch,               
                '-iter', iteration)
                

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1,2,1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1,2,2)
    plt.imshow(output_image,cmap="gist_gray")
    # plt.show()
    
# run_ndsafir(patch=3, iteration=4)
interact_manual(run_ndsafir,patch=(1,4),iteration=(1,7))
