import sys
sys.path.append("../../bioimagepy")
from bioimagepy.process import BiProcess

import imageio
from ipywidgets import interact_manual
import matplotlib.pyplot as plt

from IPython.display import clear_output

clear_output()
