output_image=None
def run_hv(denoisep,sparsep):
    global output_image
    myprocess=BiProcess('../../toolshed/cimgdenoising/Denoising_HV.xml')
    output_image=myprocess.run('-i', input_image,
                '-denoisep', denoisep,               
                '-sparsep', sparsep)
                

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1,2,1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1,2,2)
    plt.imshow(output_image,cmap="gist_gray")
    # plt.show()
    
interact_manual(run_hv,denoisep=(0, 15),sparsep=(0.1,0.9,0.01))
