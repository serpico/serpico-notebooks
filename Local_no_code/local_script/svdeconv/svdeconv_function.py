output_image = None
def run_svdeconv2d(sigma, regularization, weighting, method):
    global output_image
    myprocess = BiProcess('../../toolshed/svdeconv/svdeconv2d.xml')
    output_image = myprocess.run('-i', input_image,
                '-sigma', sigma,               
                '-regularization', regularization, 
                '-weighting', weighting,
                '-method', method) 

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap="gist_gray")
    plt.show()
    
# run_svdeconv2d(sigma=2, regularisation=10, weighting=0.5, method='SV')
interact_manual(run_svdeconv2d, sigma=(1.0, 4.0, 0.5) , regularization=(1,22), weighting=(0.1, 0.9, 0.1), method=['SV', 'HSV'])
