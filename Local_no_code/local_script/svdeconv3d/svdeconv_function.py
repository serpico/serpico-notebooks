# Interactive parameters selection
output_image = None
def run_svdeconv3d(regularization, weighting, method):
    global output_image
    myprocess = BiProcess('../../toolshed/svdeconv/svdeconv3d.xml')
    output_image = myprocess.exec('-i', input_image_path, 
                '-psf', psf_image_path,
                '-regularization', regularization, 
                '-weighting', weighting,
                '-method', method,
                '-o', '../Output/sv3D.tif')
    
    clear_output()

    print("Operation Finished")
    
interact_manual(run_svdeconv3d, regularization=(1,60), weighting=(0.1, 0.9, 0.1), method=['SV', 'HSV'])
