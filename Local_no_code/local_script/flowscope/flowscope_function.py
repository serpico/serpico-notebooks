output_image = None
filepath1 = None
filepath2 = None
filepath3 = None
input_basename3 = None

def run_flowscope(gamma, maximum_iteration):
    global output_image
    global filepath1
    global filepath2
    global filepath3
    global input_basename3
    
    input_basename1 = ntpath.basename(input_image_path1)
    input_basename2 = ntpath.basename(input_image_path2)
    input_basename3 = (ntpath.basename(input_image_path1) + "_" + ntpath.basename(input_image_path2))
    
    myprocess = BiProcess('./../../toolshed/flowscope/flowscope.xml')
    output_image = myprocess.exec('-s', input_image_path1,
                '-t', input_image_path2,
                '-g', gamma,
                '-i', maximum_iteration,
                '-o', './../Output')

    #clear_output()
    print('Job done. Your output images are in the "Output" folder')

interact_manual(run_flowscope, gamma = (0, 2.4), maximum_iteration = (0,12))
