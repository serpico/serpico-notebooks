# Import the libraries
import sys
sys.path.append("../../bioimagepy")
from bioimagepy.process import BiProcess, bi_io_print
from bioimagepy.core import BiConfig

import imageio
import numpy as np
from ipywidgets import interact_manual
import matplotlib.pyplot as plt

from IPython.display import clear_output

clear_output()
