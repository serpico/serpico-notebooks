# run the process
myprocess=BiProcess('../../toolshed/python/wilcoxon.xml')
myprocess.setConfig(BiConfig("../../toolshed/config.json"))
output=myprocess.run('-x', a, '-y', b)
clear_output()

# display outputs
print("statistic = ")
bi_io_print(output['-t'])
print("p-value = ")
bi_io_print(output['-p'])

