output_image = None
def run_atlas(rad, pval):
    global output_image
    myprocess = BiProcess('../../toolshed/atlas/atlas.xml')
    output_image = myprocess.run('-i', input_image,
                '-rad', rad,               
                '-pval', pval) 

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap="gist_gray")
    plt.show()
    
interact_manual(run_atlas, rad=(1, 40) , pval=(0.01,1,0.001))
