import sys
sys.path.append('../../bioimagepy') # change it to the directory where ATLAS is installed

from bioimagepy.process import BiProcess
import imageio
import matplotlib.pyplot as plt
from ipywidgets import interact_manual

from PIL import Image
import numpy as np
import glob
import cv2

from IPython.display import clear_output

clear_output()
print('Operation finished')
