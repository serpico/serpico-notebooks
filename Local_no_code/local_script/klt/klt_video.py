img = Image.open('../Output/kltoutput.tif')
imnp=np.array(img)

n = 1
while True:
    try:
        img.seek(n)
        img.save("../Video/frame_{}.tif".format(n))
        n = n+1
    except EOFError:
        break;

print("There are {} images in the sequence.".format(n))

images_array = []
for i in glob.glob('../Video/*.tif'):
    images=cv2.imread(i)
    height, width, layers = images.shape
    size = (width,height)
    images_array.append(images)

out = cv2.VideoWriter('../Video/project.avi',cv2.VideoWriter_fourcc(*'DIVX'), 1, size)

for p in range(len(images_array)):
    out.write(images_array[p])
out.release()



cap = cv2.VideoCapture('../Video/project.avi')
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
 
    # Display the resulting frame
    cv2.imshow('Frame',frame)
 
    # Press Q on keyboard to  exit
    if cv2.waitKey(100) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
cv2.destroyAllWindows()

print("You will find the video (project.avi) file in the 'serpico-notebooks/Vidéo' folder.")
