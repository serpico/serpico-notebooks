output_image = None
def run_klt(number, bits):
    global output_image
    myprocess = BiProcess('../../toolshed/kltracker/KLTracker.xml')
    output_image = myprocess.exec('-i', input_image_path,
                '-nf', number,               
                '-bits', bits,
                '-o', '../Output/kltoutput') 

    #clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(imageio.imread('../Output/kltoutput.tif'))
    plt.show()
    
interact_manual(run_klt, number=(0, 200) , bits=(0,32,8))
