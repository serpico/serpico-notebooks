# Interactive parameters selection
output=None
def run_process(threshold):
    global output_image
    myprocess=BiProcess('../../toolshed/fiji/threshold_particles.xml')
    myprocess.setConfig(BiConfig("../../toolshed/config.json"))
    output=myprocess.run('-input', input_image,
                '-threshold', threshold)
                

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1,2,1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1,2,2)
    plt.imshow(imageio.imread(output["-draw"]))
    
    print("particles count = ")
    bi_io_print(output["-count"])
    print("particles measures = ")
    bi_io_print(output["-measures"])
    # plt.show()
    
# run_ndsafir(patch=3, iteration=4)
interact_manual(run_process,threshold=['Default dark', 'Huang dark', 'Intermodes dark', 'IsoData dark', 'Li dark', 'MaxEntropy dark', 'Mean dark', 'MinError dark', 'Minimum dark', 'Moments dark', 'Otsu dark', 'Percentile dark', 'RenyiEntropy dark', 'Shanbhag dark', 'Triangle dark', 'Yen dark'])
