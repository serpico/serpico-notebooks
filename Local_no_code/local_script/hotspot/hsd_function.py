output_image = None
def run_hotspot(patch, neighborhood, p_value):
    global output_image
    myprocess = BiProcess('../../toolshed/hotspotdetection/HotSpotDetection.xml')
    output_image = myprocess.run('-i', input_image,
                '-patch', patch,               
                '-neighborhood', neighborhood,
                '-p_value', p_value) 

    clear_output()
    plt.figure(figsize = (10,80))
    plt.subplot(1, 2, 1)                      
    plt.imshow(input_image,cmap="gist_gray")
    plt.subplot(1, 2, 2)
    plt.imshow(output_image,cmap="gist_gray")
    plt.show()
    
interact_manual(run_hotspot, patch=(1, 6) , neighborhood=(0,10), p_value=(0,0.4,0.001))

