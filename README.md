# Serpico notebooks

This repository contains the demo notebooks of the Serpico tools. Tools are
ran locally in your computer using BioImagePy a BioImageIT tool.

To run the notebooks, you need to install the serpico tools locally or in a docker image.


## Install locally

Jupyter have to be pre-installed in you machine.
Install comands using the recomended directory architecture:

```
cd /your/working/directory
mkdir bioimageit
cd bioimageit
git clone git@gitlab.inria.fr:serpico/bioimagepy.git
git clone git@gitlab.inria.fr:serpico/serpico-toolshed.git
cd serpico-toolshed
chmod u+x package.sh
./package.sh
git clone git@gitlab.inria.fr:serpico/serpico-notebooks.git
```

run commands:
```
cd serpico-notebooks
jupyter notebook
```

To run localy, please follow the instruction at https://gitlab.inria.fr/serpico/serpico-toolshed to install the Serpico toolshed and the BioImagePy library

Please follow the instructions below to install the Serpico tools


## install using the docker image

the BioImageIT docker image is not yet availabe on docker hub. Thus, you need to build it. 
Follow the instruction at https://gitlab.inria.fr/serpico/bioimagedocker and mount your local `serpico-toolshed` directory to the /app/data of the docker image
